# BgConvert #

Some useful methods to convert identifiers

## License ##

Apache Commons 2.0

## Installation ##

        $ pip install bgconvert

## Simple usage ##

```
#!python

from bgconvert import symbol_to_ensembl, ensembl_to_symbol

ensembl_id = symbol_to_ensembl('kdm6a')
symbol_id = ensembl_to_symbol('ENSG00000147050')
```

## Your own mapping file

You can register your own map. The map file is a two column tabbulated file without headers, the first column is the ENSEMBL id and the second the symbol id. Optionally, it can be compressed using '.xz', '.gz' or '.bz2'.

```
#!python
import bgconvert

# Register a new map
bgconvert.register_ensembl_symbol_map('mymap', 'mymap.txt')

# Use this map
bgconvert.use_ensembl_symbol_map('mymap')

# Map a gene using 'mymap'
ensembl_id = bgconvert.symbol_to_ensembl('mygene')

# Switch back to the default map
bgconvert.use_ensembl_symbol_map('ensembl75')
ensembl_id = bgconvert.symbol_to_ensembl('TP53')
```

## Multiple matches or zero matches

You can control the behaviour when there are multiple matches or no matches.
```
#!python

# Return a None when there is no match (default is an empty string)
ensembl_id = bgconvert.symbol_to_ensembl('WORST_GENE_NAME_EVER', no_match=None)

# By default if there are multiple matches bgconvert will join them using a comma, 
# this example uses new line as separator:
ensembl_id = bgconvert.symbol_to_ensembl('multimatchgene', separator='\n')

# The separator can also be a function. 
# This example returns only the first match
ensembl_id = bgconvert.symbol_to_ensembl('multimatchgene', separator=lambda x: x[0])
```